'use strict';

/*
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов
*/

/**
 * Задание 1
 * Необходимо сделать выпадающее меню.
 * Меню должно открываться при клике на кнопку,
 * закрываться при клике на кнопку при клике по пункту меню
*/

const menu = document.querySelector('.menu');
const menuList = document.querySelector('.menu-list');

// Открыть и закрывает выпадающее меню по клику на кнопку
menuBtn.addEventListener('click', () => {
  menu.classList.toggle('menu-opened');
})

// Закрыть выпадающее меню по клику на пункт меню
menuList.addEventListener('click', () => {
  menu.classList.remove('menu-opened');
})

/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
*/

let rectCoordX = movedBlock.getBoundingClientRect().x;
let rectCoordY = movedBlock.getBoundingClientRect().y;

// Переместить квадрат в место клика на поле
field.addEventListener('click', (event) => {
  movedBlock.style.left = `${event.clientX-rectCoordX-25}px`;
  movedBlock.style.top = `${event.clientY-rectCoordY-25}px`;
})

/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
*/

// Скрыть сообщение при нажатии на кнопку
messager.addEventListener('click', (event) => {
  const message = event.target;
  const classList = message.classList;
  if (classList.contains('remove')) {
    message.parentElement.style.display = 'none';
  }
})

/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
*/

// Вывод уточняющего сообщения при нажатии
// на любую ссылку в document
document.addEventListener('click', (event) => {
  if (event.target.tagName === 'A') {
    const link = event.target.getAttribute('href');
    const answer = confirm(`Вы точно хотите перейти по ссылке ${link}`);
    if (answer === false) {
      event.preventDefault();
    }
  }
})

/**
 * Задание 5
 * Необходимо сделать так, чтобы значение заголовка изменялось в соответствии с измением
 * значения в input
*/

const title = taskHeader.textContent;

// Вывод содержимого поля input в заголовке
fieldHeader.addEventListener('input', () => {
  taskHeader.textContent = `${title} (${fieldHeader.value})`;
  if (fieldHeader.value === '') {
    taskHeader.textContent = title;
  }
})